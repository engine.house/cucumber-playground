const { Given, When, Then, And, But } = require('cucumber')

const JustificationCard = require('../../page-objects/JustificationCard')

Given('that the user can see the Justification card link',
  async function () {
    this.justificationCard = new JustificationCard(this.driver)

    await this.justificationCard.load()
  }
)

When('they select the Justification card link',
  async function () {
    await this.justificationCard.clickLink()
  }
)

Then('they will see the Justification card showing the justification text for the application',
  async function () {
    await this.justificationCard.checkCardContainsJustificationText()
  }
)

Then('they will see a Message Applicant Option',
  async function () {
    await this.justificationCard.checkCardContainsActions()
    await this.justificationCard.checkActionsContainsMessageApplicantOption()
  }
)

Then('they will see a Pass option',
  async function () {
    await this.justificationCard.checkCardContainsActions()
    await this.justificationCard.checkActionsContainsPassOption()
  }
)
