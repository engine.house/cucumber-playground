# B A S E
# =======
#
# This is the base image. It should be defined exactly as it will be deployed.
#
FROM node:12.4.0-alpine as base

# Create app directory and set as working directory:
#
WORKDIR /usr/src/app

# The Node image should really have set this:
#
ENTRYPOINT ["node"]

# C U C U M B E R
# ===============
#
# This is a testing layer on top of the deployable image. It should be as
# minimal as possible.
#
FROM base as cucumber-test

# Install any required test tools:
#
ENV NODE_ENV=test
RUN npm i -g cucumber@^5.1.0

ENTRYPOINT ["cucumber-js"]
