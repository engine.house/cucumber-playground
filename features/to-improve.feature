Feature: The user can see a Justification card
  If the user selects a link to a Justification card then they will see a Justification card
  with text for the application, a Message Application option, and a Pass option.

Scenario: The user can view a Justification card
  Given that the user can see the Justification card link
  When they select the Justification card link
  Then they will see the Justification card showing the justification text for the application
  And they will see a Message Applicant Option
  And they will see a Pass option

