const assert = require('assert')
const path = require('path')

const { Builder, By } = require('selenium-webdriver')

/**
 * The idea of a 'JustificationCard' class is that it represents the *concept*
 * of a Justification Card, without exposing any specifics, such as paths to
 * elements. External uses of the class just use exposed methods to test for
 * the presence of features, and if ever the structure of the card changes,
 * this would be the only place where the changes would need to be made.
 */
class JustificationCard {
  constructor(driver) {
    this.driver = driver
  }

  async load() {
    const fileName = path.join('file://', __dirname, '../src/to-improve.html')
    await this.driver.get(fileName)

    /**
     * There is some confusion in the description between the 'link' to the card and the card
     * itself. The text differentiates the two, but there is only one XPath. I'm going to
     * pretend that they are two different things, but I'm not going to bother changing
     * XPath statements, etc., because I don't know enough about the application that is
     * being tested.
     */
    this.link = await this.driver.findElement(By.xpath(
      '/html/body/app-root/ion-app/ion-router-outlet/app-work-list-router/ion-content/ion-grid/ion-row/ion-col[2]/app-work-list-detail/ion-grid/ion-row/ion-col[1]/vmd-justification-card'
    ))
    assert.ok(this.link)
  }

  async clickLink() {
    this.link.click()
    this.card = await this.driver.findElement(By.xpath(
      '/html/body/app-root/ion-app/ion-router-outlet/app-work-list-router/ion-content/ion-grid/ion-row/ion-col[2]/app-work-list-detail/ion-grid/ion-row/ion-col[1]/vmd-justification-card'
    ))
    assert.ok(this.card)
  }

  async checkActionsContainsMessageApplicantOption() {
    const actions = await this.driver.findElement(By.xpath('//*[@class="vmd-card-actions"]'))
    assert.ok(actions)
    const text = await actions.findElement(By.xpath('./ion-button[text()="Message applicant"]'))
    assert.ok(text)
  }

  async checkActionsContainsPassOption() {
    const actions = await this.driver.findElement(By.xpath('//*[@class="vmd-card-actions"]'))
    assert.ok(actions)
    const text = await actions.findElement(By.xpath('./ion-button[text()="Pass"]'))
    assert.ok(text)
  }

  async checkCardContainsActions() {
    const actions = await this.driver.findElement(By.css('.vmd-card-actions'))
    assert.ok(actions)
  }

  async checkCardContainsJustificationText() {
    const content = await this.driver.findElement(By.xpath('//*[@class="vmd-card-content"]'))
    assert.equal(await content.getText(), 'Lorem Ipsum is simply dummy text')
  }
}

module.exports = JustificationCard
