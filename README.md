# cucumber-playground

A playground...for Cucumber.

## Installation

Ideally we'd just use Docker Compose, but that is a work in progress. So instead, clone this repo, and then run `npm install`:

```shell
git clone https://gitlab.com/engine.house/cucumber-playground.git
cd cucumber-playground
npm install
```

The tests use Chrome so Selenium WebDriver will need a Chrome driver. This should be downloaded and then the `PATH` environment variable updated to refer to it.

To illustrate, assuming you have Chrome 74 installed; navigate to [the list of Chrome drivers](http://chromedriver.storage.googleapis.com/index.html) and then select '74.0.3729.6'. Download and unpack the zip file, place it somewhere convenient (such as `./bin/drivers`) and then set `PATH` to refer to it:

```shell
export PATH=./bin/drivers:$PATH
```

## Running the Tests

We can run the tests with:

```shell
npm test
```
