Feature: The user can see a specific ingredient in the ingredients detail card
  If the user selects the ingredients card they will be able to see a specific ingredient.

Background:
  Given the user has logged in

Scenario Outline: The user can see <ingredient> in the ingredients detail card
  Given the user can see the ingredients tab
  When the user clicks on the ingredients tab
  Then the user can see the ingredients list
  And the user can see <ingredient> in the ingredients list items
  When the user clicks on the <ingredient> item from the ingredients list
  Then the user can see <ingredient> in the ingredients detail card

  Examples:
    | ingredient |
    | apples |
    | cinnamon |
