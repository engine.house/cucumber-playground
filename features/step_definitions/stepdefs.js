const assert = require('assert')
const path = require('path')

const { Given, When, Then } = require('cucumber')

/**
 * TODO(MRB): Decide whether to attach this in the 'Before' hook.
 */
const { By } = require('selenium-webdriver')

/**
 * G I V E N
 * =========
 */

/**
 * Check whether the user has logged in.
 *
 * For now we're just checking that the test page can be loaded.
 */
Given('the user has logged in', async function () {
  const fileName = path.join('file://', __dirname, '../../src/cinnamon.html')
  await this.driver.get(fileName)
})

/**
 * Check whether the ingredients tab is available.
 *
 * TODO(MRB): Should really check that it's visible.
 */
Given('the user can see the ingredients tab', async function () {
  const tab = await this.driver.findElement(By.css('vmd-tab'))
  assert.equal(await tab.getText(), 'Ingredients')

  /**
   * Keep track of the ingredients tab for use by other steps:
   */
  this.ingredientsTab = tab
})

/**
 * W H E N
 * =======
 */

/**
 * Click on the ingredients tab.
 */
When('the user clicks on the ingredients tab', async function () {
  await this.ingredientsTab.click()
})

/**
 * Click on an ingredient in the ingredients list.
 */
When('the user clicks on the {word} item from the ingredients list', async function (ingredient) {
  /**
   * The label in the page has a capitalised first letter:
   */
  const label = ingredient.charAt(0).toUpperCase() + ingredient.slice(1)

  /**
   * Search for the heading relative to the root of the list:
   */
  const item = await this.ingredientsList.findElement(By.xpath(`.//h3[text()='${label}']`))

  /**
   * Now click on it:
   */
  await item.click()
})

/**
 * T H E N
 * =======
 */

/**
 * Check that a detail card is available for some specified ingredient.
 */
Then('the user can see {word} in the ingredients detail card', async function (ingredient) {
  /**
   * The label in the detail card has a capitalised first letter:
   */
  const label = ingredient.charAt(0).toUpperCase() + ingredient.slice(1) + ' Details'

  /**
   * Search for the heading inside a detail card:
   */
  await this.driver.findElement(By.xpath(`//vmd-ingredients-detail-card//vmd-card[@title='Ingredients Details']//h3[text()='${label}']`))
})

/**
 * Check that the ingredients list is available.
 */
Then('the user can see the ingredients list', async function () {
  this.ingredientsList = await this.driver.findElement(By.css('vmd-ingredients-list'))
})

/**
 * Check that some specified ingredient is actually in the list
 */
Then('the user can see {word} in the ingredients list items', async function (ingredient) {
  /**
   * The label in the page has a capitalised first letter:
   */
  const label = ingredient.charAt(0).toUpperCase() + ingredient.slice(1)

  /**
   * Search for the heading relative to the root of the list:
   */
  await this.ingredientsList.findElement(By.xpath(`.//h3[text()='${label}']`))
})
